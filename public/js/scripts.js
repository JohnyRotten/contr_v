$(function(){
	function module(str) {
		$('body').append('<div id="module">' + str + '</div>');
		var module = $('#module');
		module.css({
			marginTop:  -module.width()/2,
			marginLeft: -module.height()/2
		});
		module.animate({opacity: 1}, 300);
	}

	$('body').on('click', '#module', function(){
		$('#module').animate({ opacity: 0 }, 300);
		setTimeout(function(){ $('#module').remove() }, 300);
		return false;
	});

	$('body').on('click', 'a[href^="#"]', function(){
		var curr = 0;
		var curr_block = $(this).attr('href');
		var width = 960;
		curr = $('.main_block').index($(curr_block));
		$('#blocks').animate({left: -width*curr}, 300);
		return false;
	});
	if ($('#video_link').length) {
		$('body').on('click', '#video_link', function(){
			var v = "<div id=\"video_wrapper\">" +
					    "<video id=\"video\" autoplay=\"true\">" +
					        "<source src=\"/video/preview.mp4\"/>" +
					        "<source src=\"/video/preview.ogv\" type='video/ogg; codecs=\"theora, vorbis\"' />" +
					        "<source src=\"/video/preview.webm\" type='video/webm; codecs=\"vp8, vorbis\"' />" +
					        "<p>Video tag not supported.</p>" +
					    "</video>" +
					"</div>";
			$('body').append(v);
			video.volume = 0.3;
			return false;
		});
		$('body').on('click', '#video_wrapper', function(){
			var delay = 500;
			if (video !== undefined) {
                video.pause();
                video.currentTime = 0;
            }
            $('#video_wrapper').animate({'opacity': 0}, delay);
            setTimeout(function(){
                $('#video_wrapper').remove();
	            $('#audio_control').removeClass('off').addClass('on');
	            sound.play();
            }, delay);
			return false;
		});
        $('#video_link').trigger('click');
        setInterval(function(){
            $('#video_wrapper').trigger('click');
        }, 20000);
	}

	$('#slider').parent().append('<a class="prev"><</a><a class="next">></a>');

	$('body').on('click', 'a.prev', function(){
		var cur   = $('#slider').data('current') - 1;
		var width = 800;
		if (cur < 0) cur = $('.slide').length - 1;
		$('#slider').data('current', cur);
		$('#slider').animate({ left: -cur * width }, 300);
		console.log(cur + ' from ' + $('.slide').length);
		return false;
	});
	$('body').on('click', 'a.next', function(){
		var cur   = $('#slider').data('current') + 1;
		var width = 800;
		cur %= $('.slide').length;
		$('#slider').data('current', cur);
		$('#slider').animate({ left: -cur * width }, 300);
		console.log(cur + ' from ' + $('.slide').length);
		return false;
	});

	$('body').on('submit', '#contact form', function(){
		var errors  = [];
		var from    = $('#from').val();
		var subject = $('#subject').val();
		var msg     = $('#msg').val();

		var reg_email = /^([a-z]+[\w\-\._]*)@([a-z]+[\w\-\._]*\.[a-zA-Z]{2,4})$/i;
		

		if (!from) {
			errors[errors.length] = 'Поле email не заполнено.';
		} else if (!reg_email.test(from)) {
			errors[errors.length] = 'Email указан не верно.';
		}

		if (!subject) {
			errors[errors.length] = 'Тема не указана.';
		} 

		if (!msg) {
			errors[errors.length] = 'Не заполнен текст сообщения.';	
		}

		if (!errors.length) {
			$.ajax({
				method: 'post',
				url: '/send_alarm',
				data: {
					'from':    from,
					'subject': subject,
					'message': msg
				}
			}).done(function(responce){
				module(responce);
			});
		} else {
			module('<h1>Ошибка!</h1><br/>' + errors.join('<br>'));
		}
		return false;
	});

    $('body').on('click', '#audio_control', function(){
        if ($(this).hasClass('on')) {
            $(this).removeClass('on').addClass('off');
            sound.pause();
        } else {
            $(this).removeClass('off').addClass('on');
            sound.play();
        }
        return false;
    });
});