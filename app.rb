#! /usr/ruby -w
# coding: utf-8
require 'sinatra'
require './helpers/storage.rb'
require './helpers/alarm.rb'

storage = Storage.new

get '/' do
	@menu_items = storage.menu_items
	@services   = storage.services

	erb :landing, locals: { title: '"Контр-В" | Главная страница' } 
end

post '/send_alarm' do
	begin
		send_alarm 'contr-v@mail.ru', params[:from], params[:subject], params[:msg]
		'<h1>Сообщение отправлено.</h1>'
	rescue => er
		"<h1>Ошибка</h1><p>Непредвиденая ошибка.</p>"
	end
end

not_found do
	status 404
	@menu_items = []
	@error = {
		code:    404,
		message: "Страница не найдена."
	}
	erb :error, locals: { title: @error[:message] }
end

set :public_folder, File.dirname(__FILE__) + '/public'
