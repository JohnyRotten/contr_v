#! /usr/bin/ruby
# coding: utf-8

require 'net/smtp'

def send_alarm(to, from, subject, text)
	message=<<MSG
	From: Admin <#{from}>
	To: #{to}
	Subject: #{subject}
	Body: 
		#{text}
MSG
	Net::SMTP.new('localhost', 25).start('example.com') do |smtp|
		smtp.send_message message, from, to
	end
end
