#! /usr/bin/ruby -w
# coding: utf-8
require './helpers/database.rb'

class Storage
	attr_accessor :services, :menu_items

	def initialize
		@services   = Database.getServices
		@menu_items = Database.getMenuItems
	end
end